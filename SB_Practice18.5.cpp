﻿#include <iostream>
#include <algorithm>

class Player
{
public:
    std::string name;
    int score;

    Player() : name(""), score(0) {}
    Player(const std::string& playerName, int playerScore) : name(playerName), score(playerScore) {}
};

bool comparePlayers(const Player& a, const Player& b)
{
    return a.score > b.score;
}

int main()
{
    setlocale(LC_ALL, "Russian");

    std::cout << "Колличество игроков: ";
    int numPlayers;
    std::cin >> numPlayers;

    Player* players = new Player[numPlayers];


    for (int i = 0; i < numPlayers; ++i)
    {
        std::cout << "Имя игрока " << i + 1 << ": ";
        std::cin >> players[i].name;

        std::cout << "Сколько очков набрал игрок " << i + 1 << ": ";
        std::cin >> players[i].score;
    }

    std::sort(players, players + numPlayers, comparePlayers);

    setlocale(LC_ALL, "Russian");
    std::cout << "Рейтинг игроков:\n";
    for (int i = 0; i < numPlayers; ++i) {
        std::cout << "Место " << i + 1 << ": " << players[i].name << " - " << players[i].score << " очков\n";
    }

    delete[] players;

    return 0;
}
